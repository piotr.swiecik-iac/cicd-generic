# cicd-generic



## Overview

Generic CICD setup based on Jenkins. Infrastructure runs on single machine provisioned as local Vagrant VM or remote EC2 instance.

## Components

- [ ] Jenkins controller
- [ ] Jenkins agent
- [ ] Artifactory
- [ ] Sonarqube